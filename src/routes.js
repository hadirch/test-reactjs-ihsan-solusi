import Register from "../src/component/Auth/register"; 
import {
    Switch,
    Route,
    Redirect,
 } from "react-router-dom";
import { UserContext, } from "../src/component/Auth/userContext";
import { useContext } from "react";
import Login from "./component/Auth/login";
import DetailProduct from "./component/Product/productDetail";
import ProductForm from "./component/Product/productForm";
import ProductsTable from "./component/Product/productTable";
 const Routes = () => {
    const {
        userState:[user],
      } = useContext(UserContext)
        const PrivateRoute = ({...rest})=>{
            if (user){
                return <Route {...rest}/>
            }else{
                return <Redirect to="/login"/>
            }
        }
        const LoginRoute = ({...rest})=>{
            if (user){
                return <Redirect to="/"/>
            }else{
                return <Route {...rest}/>
            }
        }
    
    return(          
        <div> 
            <div className="App">
                <Switch>
                    <Route exact path="/">
                    <ProductsTable/>
                    </Route>
                    <LoginRoute exact path="/register">
                        <Register/>
                    </LoginRoute>
                    <LoginRoute exact path="/login">
                        <Login/>
                    </LoginRoute>
                    <Route exact path="/product/:slug">
                        <DetailProduct/>
                    </Route>
                    <PrivateRoute exact path="/product-form">
                        <ProductForm/>
                    </PrivateRoute>
                    <PrivateRoute exact path="/product-form/:id/edit">
                        <ProductForm/>
                    </PrivateRoute>
                    <PrivateRoute exact path="/product-table">
                        <ProductsTable/>
                    </PrivateRoute>
                    <PrivateRoute exact path="/product-detail/:slug">
                        <DetailProduct/>
                    </PrivateRoute>
                </Switch>
            </div>
        </div>       
    )
 }

 export default Routes