import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { Link, useParams,useHistory } from 'react-router-dom';
import { Button } from 'antd';
import { UserContext } from '../Auth/userContext';
import { logout } from '../utils/function';

const DetailProduct = () => {
    let { slug } = useParams()
    let history = useHistory()
    const [product, setProduct] = useState({})
    const {
        userState:[user, setUser]
      } = useContext(UserContext)
    const url = "https://cms-admin.ihsansolusi.co.id/testapi"
    useEffect(() => {

        if (slug !== undefined) {
    
            axios.get(`${url}/user/${slug}`,{headers: {"Authorization" : "Bearer "+ user.token}})
                .then((res) => {
                    let { data } = res
                    setProduct(data)
                    console.log(data)
                    console.log("tes")
                })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return (
        <>
            <div className='wrap-1'>
            <div className="container-1">
                <div className="text-1">
                {console.log(product.data?.name)}
                {console.log("tes1")}
                  <div className="judul">{product.data?.productName}</div>
                    <p>Name : {product.data?.name}</p>
                    <p>Alamat: {product.data?.address}</p>
                    <p>Tanggal lahir: {product.data?.born_date}</p>
                    <p>Jenis Kelamin: {product.data?.gender === "l" ? "Pria" : "wanita"}</p>
                    <p>Tanggal Input: {product.data?.created_at}</p>
                </div>
            </div>
            </div>
        </>
    )
};

export default DetailProduct;