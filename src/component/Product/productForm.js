import { useState, useEffect, useContext } from "react"
import axios from "axios"
import { useHistory, useParams } from "react-router-dom"
import { UserContext } from "../Auth/userContext"
import { logout } from "../utils/function"
import { Button, DatePicker, Radio } from "antd"
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
dayjs.extend(customParseFormat);


const dateFormat = 'YYYY-MM-DD';
console.log(dateFormat)
console.log("dateFormat")


const ProductForm = () => {
    let {id} = useParams()
    let history = useHistory()
    const url = "https://cms-admin.ihsansolusi.co.id/testapi"
    let productObj={
        name: "", 
        address: "", 
        gender: "", 
        born_date: "",
        id: null,
    }
    const [input, setInput] = useState(productObj)
    const [currendId,setCurrentId] = useState(null)
    const {
        userState:[user, setUser]
      } = useContext(UserContext)
    const [confirmLoading, ] = useState(false)

    useEffect(()=>{
        const fecthData = async () => {
            let result = await axios.get(`${url}/user/${id}`, {headers: {"Authorization" : "Bearer "+ user.token}})
            let currentProduct = result.data
            console.log(currentProduct)
            console.log("currentProduct")
            setCurrentId(id)
            setInput( 
            {
                id: currentProduct.id,
                name: currentProduct.name,
                address: currentProduct.address,
                gender: currentProduct.gender,
                born_date: currentProduct.born_date                
            }
        )}
        if (id){
            fecthData()
        }
    },[id])

    const handleSubmit = (event)=>{
        event.preventDefault()
        if (currendId === null) {
            axios.post(`${url}/user`,{
                id: input.id,
                name: input.name,
                address: input.address,
                gender: parseInt(input.gender),
                born_date: input.born_date}
                    ,{headers: {"Authorization" : "Bearer "+ user.token}}).then((res)=>{
                let data = res.data
                console.log(data)
                console.log("datasssssssss")
                history.push("/product-table")
            }).catch((err) => {
                console.log(err);
                const {status} = err.response.data
                if (status === "Token is Invalid" || status === "Token is Expired") {
                    logout({setUser, status: "expired"})
                }
            } )
            
        } else {
            axios.put(`${url}/user/${currendId}`,{
                id: input.id,
                name: input.name,
                address: input.address,
                gender: input.gender,
                born_date: input.born_date
            }, {headers: {"Authorization" : "Bearer "+ user.token}}).then((res)=>{
                let data = res.data 
                console.log(data);
                history.push("/")
            }).catch((err)=>{
                console.log(err.response.data);
                const {status} = err.response.data
                if (status === "Token is Invalid" || status === "Token is Expired") {
                    logout({setUser, status: "expired"})
                }
            })
        }

        setCurrentId(null)
        setInput(productObj)
    }
    const handleChange = (event) => {
        let value = event.target.value
        let title = event.target.name
        console.log(title);
        setInput({
            ...input,
            [title]:value
        })
    }

    const handleBack = ()=>{
        history.push("/") //untuk berpindah ke form
    }
    return(
            <>  
                <div id="form" className="hide-display" style={{ marginTop: 30, padding: 50, backgroundColor: 'white', textAlign:'center', height: '680px' }}>
                    <h1>{id === undefined ? `Input Product Baru` :`Form Edit ${input.name}`}</h1>
                    <form onSubmit={handleSubmit}>
                        <table style={{ marginLeft:350 }}>
                            <tr>
                                <td>
                                    <strong style={{ color: "#0f0707" }}>Name : </strong>
                                </td>
                                <td>
                                    <input style={{ width: 400 }} value={input.name} onChange={handleChange} name="name" required /><br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong style={{ color: "#0f0707" }}>address :</strong>
                                </td>
                                <td>
                                    <input rows={4} style={{ width: 400 }} value={input.address} onChange={handleChange} name="address" required /><br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong style={{ color: "#0f0707" }}>gender : </strong>
                                </td>
                                <td>
                                <Radio.Group 
                                name="gender" 
                                onChange={handleChange}
                                id="gender"
                                value={input.gender}
                                >
                                    <Radio value={`l`} >Pria</Radio>
                                    <Radio value={`p`} >Wanita</Radio>
                                </Radio.Group>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong style={{ color: "#0f0707" }}>born_date : </strong>
                                </td>
                                <td>
                                    {/* <DatePicker onChange={handleChange} name="born_date" required/> */}
                                    <DatePicker defaultValue={dayjs('', dateFormat)} format={dateFormat} onChange={handleChange} name="born_date" required />
                                </td>
                            </tr>
                        </table>
                        <Button onClick={handleBack} type="danger" block htmlType="submit" style={{ height:50, width:'auto', borderRadius:'7px',marginRight:'50px'}}>
                            Cancel
                        </Button>
                        <Button loading={confirmLoading} type="primary" block htmlType="submit" style={{ background: "#06283D",width:'auto', height:50, borderRadius:'7px', margin:'10px 0'}}>
                            Submit
                        </Button>
                    </form>
                        </div>
            </>

                
    )
}

export default ProductForm