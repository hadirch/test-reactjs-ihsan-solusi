import { Modal ,Layout, Button, Menu} from 'antd';
import {VideoCameraAddOutlined, EditOutlined , DeleteOutlined, EyeOutlined} from '@ant-design/icons';
import { Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react"
import { useHistory } from "react-router-dom"
import axios from "axios"
import { UserContext } from '../Auth/userContext';
const { Sider } = Layout;

const ProductsTable = () => {
    const url = "https://cms-admin.ihsansolusi.co.id/testapi"
    let history = useHistory()
    const [product,setProduct] = useState([]);
    const [fetchTrigger, setFetchTrigger] = useState(true)

    const {
        userState:[user, ],
        loadingState:[confirmLoading, setConfirmLoading]
      } = useContext(UserContext)
      const [open, setOpen] = useState(false);

    useEffect(()=>{
        const fecthData = async () => {
            let resultProduct = await axios.get(`${url}/user`, {headers: {"Authorization" : "Bearer "+ user.token}}) 
            console.log(resultProduct)
            console.log("resultProduct")
            let dataProduct = resultProduct.data
            console.log(dataProduct);
            console.log("dataProduct");
            let aneh = dataProduct.data
            setProduct(aneh);
            setProduct(aneh.map((itemProduct) =>{
                return {
                    id: itemProduct.id,
                    name: itemProduct.name,
                    address: itemProduct.address,
                    gender: itemProduct.gender,
                    born_date: itemProduct.born_date,
                    created_at: itemProduct.created_at,
                    }}
                ))
            }
            setFetchTrigger(false)
        if (fetchTrigger) {
            fecthData()
            
        }
    },[fetchTrigger])
    
    const addNewProduct = () => {
        history.push("/product-form")
    }

    const handleEdit = async (id) => {
        let idProduct = Number(id)
        history.push(`/product-form/${idProduct}/edit`)
    }
    
    const handleView = async (id) => {
        let idProduct = Number(id)
        history.push(`/product-detail/${idProduct}`)
    }

    const showModal = (event) => {
        setOpen(true);
      };
    
    const handleOk = (id) => {
      let idProduct = Number(id)
      console.log(`Ini Id ${idProduct}`);
      console.log("assu");
      setConfirmLoading(true);
      setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
      if (confirmLoading === false) {
            axios.delete(`${url}/user/${idProduct}`, {headers: {"Authorization" : "Bearer "+ user.token}}).then((res)=>{
            // setFetchTrigger(true)
            console.log(idProduct);
            console.log("assu");
            history.push(`/product-table`)
        }).catch((error)=>{
            console.log(error);
        })
        }
    }, 2000);
      };
    
    const handleCancel = () => {
        console.log('Clicked cancel button');
        setOpen(false);
      };
      // yyyy-MM-dd
const input = "2020-08-19"
const [year, month, day] =  input.split('-')
// dd/mm/yyyy
console.log(`${day}/${month}/${year}`)
console.log("qqqqq")

      function Tabel (props) {
        return(
            <tbody>
                    {props.product.map((item, index) => {
                        return (
                           <tr key={index}>
                               <td>{index+1}</td>
                               <td>{item.name}</td>
                               <td>{item.address}</td>
                               <td>{item.gender}</td>
                               <td>{item.born_date}</td>
                               <td>{item.created_at}</td>
                               <td>
                               <Button
                                    onClick={()=>{handleView(item.id)}}
                                    icon={<EyeOutlined/>}
                                    style={{background: "#A5F1E9",color:"#06283D",marginRight:10,borderRadius:"4px"}}
                                    >
                                      View
                                </Button>
                               <Button
                                    onClick={()=>{handleEdit(item.id)}}
                                    icon={<EditOutlined />}
                                    style={{background: "#FFCE45",color:"#06283D",marginRight:10,borderRadius:"4px"}}
                                    >
                                      Edit
                                </Button>
                                <Button
                                type="danger"
                                onClick={showModal}
                                value={item.id}
                                icon={<DeleteOutlined />}
                                style={{color:"white", borderRadius:"4px"}}
                                >
                                  Delete
                                </Button>
                                <Modal
                                  title="Delete This Product?"
                                  open={open}
                                  onOk={()=>{handleOk(item.id)}}
                                  confirmLoading={confirmLoading}
                                  onCancel={handleCancel}
                                >
                                  <p>Menghapus Product Dari Data</p>
                                </Modal>
                               </td>
                           </tr>
                        )
                    })
                    }
                </tbody>
        )
      }

    return(
    <div>
        <div className='title-movielist'>
        </div>
        <Button
            type="default"
            icon={<VideoCameraAddOutlined />}
            onClick={addNewProduct}
            style={{
                // left:540,
                right:390,
                marginTop:20,
                marginBottom:20,
                background: "#00C897",
                color:"#06283D",
                marginRight:10,
                borderRadius:"4px"}}
            >
              Add Product
        </Button>
        <table className="custom-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>P/W</th>
                        <th>Tanggal Lahir</th>
                        <th>Tanggal Input</th>
                        <th>Aksi </th>
                        {console.log({product})}
                        {console.log("hyhyhy")}
                    </tr>
                </thead>
                <Tabel product={product} />
            </table>
        </div>
    )
}

export default ProductsTable